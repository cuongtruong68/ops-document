## Configuring
### 1. Allow network IP access
```
/etc/squid/squid.conf
...
acl localnet src 10.0.0.0/8	    # RFC1918 possible internal network
acl localnet src 172.16.0.0/12	# RFC1918 possible internal network
acl localnet src 192.168.0.0/16	# RFC1918 possible internal network
...
http_access allow localnet
```
- acl means an Access Control List
- localnet is the name of ACL
- src is where the request would originate from under this ACL.
### 2. Caching
```
/etc/squid/squid.conf
...
cache_dir ufs /var/squid 100 16 256
cache_mem 64 MB
```
### 3. Blocking Websites
#### 3.1 Add block site to file
```
/etc/squid/blocked_sites
...
blocka.com
blockb.com
```
#### 3.2 Update configuration file
```
/etc/squid/squid.conf
...
acl blocked_sites dstdomain "/etc/squid/blocked_sites"
http_access deny blocked_sites
```
#### 3.3 Block site with time
```
/etc/squid/squid.conf
...
acl allow.domain src 192.168.1.0/24
acl surfing_hours time M T W H F 08:00-17:00
...
http_access allow allow.domain surfing_hours
http_access deny allow.domain
```
- acl surfing_hours time M T W H F 08:00-17:00: are allowing access from Monday to Friday between 08:00 and 17:00
### 4. Block a port
#### 4.1 Block particular port
```
/etc/squid/squid.conf
...
acl block_port port 1234
http_access deny block_port
```
#### 4.2 Block particular port and skip net work
```
/etc/squid/squid.conf
...
acl block_port port 1234
acl no_block_port_ip src 192.168.1.5
http_access deny block_port !no_block_port_ip
```
### 4. Logging
#### 4.1 Configuration
```
/etc/squid/squid.conf
...
debug_options ALL,1
access_log /var/log/squid/access.log squid
```
#### 4.2 Squid log files 
- /var/log/squid/cache.log
- /var/log/squid/store.log
- /var/log/squid/access.log
## Securing
### 1. Use htpasswd to generate a password for a new Squid user
```
htpasswd -c /etc/squid/passwords <squid username>
```
### 2. Use openssl to generate a password for a new Squid user
```
printf "<squid username>:$(openssl passwd -crypt '<squid passwd>')\n" | sudo tee -a /etc/squid/passwords
```
### Update configuration file
```
/etc/squid/squid.conf
...
auth_param basic program /usr/lib/squid3/basic_ncsa_auth /etc/squid/passwords
auth_param basic realm proxy
auth_param basic credentialsttl 24 hours
auth_param basic casesensitive off
acl authenticated proxy_auth REQUIRED
...
http_access allow localnet
http_access allow authenticated
...
```
- auth_param basic credentialsttl 24 hours: after 24 hours, user/pass will be asked again
- auth_param basic casesensitive off: case sensitive for user is off
## Build with Docker
#### Build images
```
docker build no-cache -t squid:v2024.01.13 .
```
## Testing
### 1. With normal
```
curl -v -x http://squid-server-ip:3128 https://google.com
```
### 2. With authentication
```
curl -v -x http://squid-user:squid-passw@squid-server-ip:3128 https://google.com
```
## Use case:
#### 1. HTTP/HTTPS proxy 
- [docker-compose.yml](./use-case/docker-compose.yml)

## Reference:
* [How to Set Up Squid Proxy for Private Connections](https://www.digitalocean.com/community/tutorials/how-to-set-up-squid-proxy-on-ubuntu-20-04)
* [Squid proxy configuration tutorial on Linux](https://linuxconfig.org/squid-proxy-configuration-tutorial-on-linux)
* [How to Restrict Web Access By Time Using Squid Proxy Server](https://webhostinggeeks.com/howto/how-to-restrict-web-access-by-time-using-squid-proxy-server-on-centos-6-2/)
* [Squid proxy How to filter or block a particular port](https://www.cyberciti.biz/faq/linux-unix-squid-proxy-filtering-particular-port/)