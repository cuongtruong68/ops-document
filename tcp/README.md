## I. TCP
#### 1. OSI Model:
* OSI stands for Open Systems Interconnection. It has been developed by ISO – ‘International Organization for Standardization‘, in the year 1984. It is a 7-layer architecture with each layer having specific functionality to perform. All these 7 layers work collaboratively to transmit the data from one person to another across the globe.
* The OSI Model is just a reference/logical model. It was designed to describe the functions of the communication system by dividing the communication procedure into smaller and simpler components. 
![](./images/iso.png)

#### 2. IPv4 - TCP/IP Model:
* TCP/IP was designed and developed by the Department of Defense (DoD) in the 1960s and is based on standard protocols. It stands for Transmission Control Protocol/Internet Protocol. The TCP/IP model is a concise version of the OSI model. It contains four layers.
* What Does TCP/IP Do? \
The main work of TCP/IP is to transfer the data of a computer from one device to another. The main condition of this process is to make data reliable and accurate so that the receiver will receive the same information which is sent by the sender. To ensure that, each message reaches its final destination accurately, the TCP/IP model divides its data into packets and combines them at the other end, which helps in maintaining the accuracy of the data while transferring from one end to another end.
![](./images/tcp-model.png)

#### 3. Ethernet Frame Format(Physical layer)
* Basic frame format which is required for all MAC implementation is defined in IEEE 802.3 standard.
![](./images/ethernet-fmt.png)
#### 4. IPv4 Datagram Header(Network layer)
![](./images/ipv4-fmt.png)
#### 5. Structure in TCP:
![](./images/tcp-struct.png)
#### 6. TCP 3-Way Handshake:
- TCP \
TCP (Transmission Control Protocol) is one of the main protocols of the Internet protocol suite. It lies between the Application and Network Layers which are used in providing reliable delivery services.
- 3-Way Handshake: \
![](./images/tcp-fin.png)


#### 7. TCP Connection Termination:
![](./images/tcp-es.png)

#### 8. Reference
1. [Layers of OSI Model](https://www.geeksforgeeks.org/layers-of-osi-model/)
2. [What is Transmission Control Protocol (TCP)?](https://www.geeksforgeeks.org/what-is-transmission-control-protocol-tcp)
3. [TCP 3-Way Handshake Process](https://www.geeksforgeeks.org/tcp-3-way-handshake-process)
4. [TCP Connection Termination](https://www.geeksforgeeks.org/layers-of-osi-model/)
5. [TCP/IP Model](https://www.geeksforgeeks.org/tcp-ip-model/)
6. [Introduction and IPv4 Datagram Header](https://www.geeksforgeeks.org/introduction-and-ipv4-datagram-header)
7. [Ethernet Frame Format](https://www.geeksforgeeks.org/ethernet-frame-format/)
8. [Services and Segment structure in TCP](https://www.geeksforgeeks.org/services-and-segment-structure-in-tcp)

## II. Need to know
#### 1. Binary number
- Bitwise Operators
- Representation
- Decimal to binary
#### 2. Hexadecimal
- Decimal to Hexadecimal
- Hexadecimal to Binary
#### 3. ASCII
#### 4. UTF-8
#### 5. Algorithm:
- base64
- hmac
- md5
- sha
#### 7. JSON
#### 8. Network
- Subnet
  * IP Subnet Calculator
- Firewall
- DNS
- Domain
- IP, Port
- Network status
  * Windows 
  * Mac
  * Linux
#### 9. Reference
1. [ASCII Table](https://www.ascii-code.com)
2. [UTF-8 code page](https://www.charset.org/utf-8)
3. [Representation of Negative Binary Numbers](https://www.geeksforgeeks.org/representation-of-negative-binary-numbers)
4. [Bitwise Operators in C/C++](https://www.geeksforgeeks.org/bitwise-operators-in-c-cpp)
5. [RapidTables](https://www.rapidtables.com/convert/number/hex-to-binary.html)
6. [Introducing JSON](https://www.json.org/json-en.html)
7. [Checking Active TCP/IP Connections on Windows with PowerShell](https://woshub.com/get-nettcpconnection-windows-powershell/)
8. [Guide To Subnet Mask (Subnetting) & IP Subnet Calculator](https://www.softwaretestinghelp.com/subnet-mask-and-network-classes/)
