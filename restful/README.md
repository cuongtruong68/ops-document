## RESTful API
### 1. RESTful API
#### 1.1 API
- An application programming interface (API) defines the rules that you must follow to communicate with other software systems - **AWS**
#### 1.2 REST API
- Representational State Transfer (REST) is a software architecture  - **AWS**
- A REST API is an API that conforms to the design principles of the REST - **IBM**

### 2. Authentication methods
- RESTful API has four common authentication methods:
    - HTTP authentication
        - Basic authentication
        - Bearer authentication
    - API keys
    - OAuth

## Reference:
1. [What is RESTful API?](https://aws.amazon.com/what-is/restful-api)
2. [What is a REST API?](https://www.ibm.com/topics/rest-apis)