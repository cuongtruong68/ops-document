# Kong Gateway
API gateway built for hybrid and multi-cloud, optimized for microservices and distributed architectures

# Konga
An open source tool that enables you to manage your Kong API Gateway with ease

## Kong Gateway Setup

### 1. Create docker network
```
docker network create kong-net
```
### 2. Initialize Postgre DB and pgAdmin
```
cd ./postgres
mkdir pgadmin
mkdir postgres
docker-compose up
```
### 3. Prepare the Kong database
```
cd ./kong-migration
docker-compose up
```
### 4. Run Kong Gateway
```
cd ./kong-gw
docker-compose up
```
### 5. Verify installation
```
curl -i -X GET --url http://localhost:8001/services
```
### 6. Verify that Kong Manager
```
http://127.0.0.1:8002
```
## Concepts
### 1. [Services](https://docs.konghq.com/gateway/latest/key-concepts/services/)
- In Kong Gateway, a service is an entity representing an external upstream API or microservice
### 2. [Routes](https://docs.konghq.com/gateway/latest/key-concepts/routes/)
- Routes determine how (and if) requests are sent to their services after they reach Kong Gateway
### 3. [Upstreams](https://docs.konghq.com/gateway/latest/key-concepts/upstreams/)
- Upstream refers to an API, application, or micro-service that Kong Gateway forwards requests to
### 4. Consumer
- Consider them users. (But since they are not always human, we prefer to call them consumers, as they 'consume' the service)
- Consumers are identified by authentication plug-ins. So adding basic auth on a service or route will allow it to identify a consumer (or block access if credentials are bad)

## Securing the Admin API
### 1. Kong API Loopback
### Assume that Kong admin_listen on 127.0.0.1:8001
#### 1.1 Creating a Service
```
curl -X POST http://127.0.0.1:8001/services \
  --data name=admin-api \
  --data host=127.0.0.1 \
  --data port=8001
```
#### 1.2 Route Service
```
curl -X POST http://127.0.0.1:8001/services/admin-api/routes \
  --data paths[]=/admin-api
```
#### 1.3 Verify Service
```
curl -X GET http://myhost.dev:8000/admin-api/services
```
#### 1.4 Service based key authentication
```
curl -X POST http://127.0.0.1:8001/services/admin-api/plugins \
    --data "name=key-auth" 
```
### 2. Reference
- [Securing the Admin API](https://docs.konghq.com/gateway/latest/production/running-kong/secure-admin-api/#main)

## Key Authentication
### 1. Enable authentication
### 2. Reference
 - [Key Authentication](https://docs.konghq.com/gateway/3.5.x/get-started/key-authentication/#)
## KongA setup
### 1. Set up consumers and keys
#### 1.1 Enable the Key Authentication plugin with Admin API (Service based key authentication)
```
curl -X POST http://127.0.0.1:8001/services/admin-api/plugins \
    --data "name=key-auth" 
```
#### 1.2 Create a new consumer
```
curl -i -X POST http://127.0.0.1:8001/consumers/ \
  --data username=konga
  --data custom_id=cebd360d-3de6-4f8f-81b2-31575fe9846a
```
#### 1.3 Assign the consumer a key
1.3.1 Assign with username
```
curl -i -X POST http://127.0.0.1:8001/consumers/konga/key-auth \
  --data key=<secret-key>
```
1.3.2. Assign with custom_id
```
curl -i -X POST http://127.0.0.1:8001/consumers/cebd360d-3de6-4f8f-81b2-31575fe9846a/key-auth
```
#### 1.4 Run Postgre 9.6
```
cd ./konga
mkdir postgres
docker-compose -f postgres96.yml up
```
#### 1.5 Run Konga
```
cd ./konga
docker-compose up
```
## Security
### 1. Prevent bot or custom clients with Bot detection plugin
- [List of rules](https://github.com/Kong/kong/blob/master/kong/plugins/bot-detection/rules.lua)
- Enable on a service
```
curl -X POST http://127.0.0.1:8001/services/1st_service/plugins \
   --data "name=bot-detection"  \
   --data "config.deny=helloworld"
```
- Testing
```
curl -v http://127.0.0.1:8000/servicea  -H "Host: kong-be" -X GET -H "User-Agent: Twitterbot1.1"
```
### 2. Remove or change respone header with Response Transformer plugin
#### 2.1. Remove common header
```
Server
Via
X-Powered-By
```
#### 2.2. Remove Google header
```
x-goog-generation
x-goog-hash
x-goog-metageneration
x-goog-storage-class
x-goog-stored-content-encoding
x-goog-stored-content-length
x-guploader-uploadid
```
### 3. Handle CORS request
- Enable on a service
```
curl -X POST http://127.0.0.1:8001/services/1st_service/plugins \
    --data "name=cors"  \
    --data "config.origins=http://kong-fe.local"  \
    --data "config.methods=GET"  \
    --data "config.headers=x-access-token"  \
    --data "config.credentials=true"  \
    --data "config.max_age=3600"
```
- Testing
```
curl -v -H "Host: kong-be" http://127.0.0.1:8000/servicea  -H "Origin: http://kong-fe.local" -H "Access-Control-Request-Method: GET" -H "Access-Control-Request-Headers: x-access-token" -X OPTIONS 
```
### 4. Allow or deny IP with IP Restriction plugin
- Enable on a service
```
curl -X POST http://127.0.0.1:8001/services/1st_ip_restriction/plugins \
    --data "name=ip-restriction"  \
    --data "config.allow=127.0.0.1"  \
```
- Testing
```
curl -v -H "Host: kong-be" http://127.0.0.1:8000/ip_restriction
```
### 5. Access control list
- Create consumer
```
curl -i -X POST http://127.0.0.1:8001/consumers/ \
  --data username=servicea
  --data custom_id=servicea
```
- Assign the consumer a key
```
curl -i -X POST http://127.0.0.1:8001/consumers/servicea/key-auth \
  --data key=<secret-key>
```
- Add consumer to group
```
aa
```
- Enable Key Auth 
```
curl -X POST http://127.0.0.1:8001/services/servicea/plugins \
   --data "name=key-auth"  \
   --data "config.key_names=apikey"
```
- Enable ACL on service to consumer group
```
curl -X POST http://localhost:8001/services/servicea/plugins \
   --data "name=acl"  \
   --data "config.allow=servicea"  \
```
## Reference:
- [Kong Gateway](https://docs.konghq.com/gateway/latest/)
- [Install Kong Gateway on Docker](https://docs.konghq.com/gateway/latest/install/docker/)
- [KongA](https://pantsel.github.io/konga/)
- [postgres - Official Image](https://hub.docker.com/_/postgres)
- [dpage/pgadmin4](https://hub.docker.com/r/dpage/pgadmin4/)
- [Setup (Kong + Konga) as API Gateway](https://dev.to/vousmeevoyez/setup-kong-konga-part-2-dan)
- [Kong Free Plugin](https://docs.konghq.com/hub/?tier=free)