### This repo is my personal knowledge and work experience since I started working in the information technology industry.
- My Linkedin profile: https://www.linkedin.com/in/khang-diy-937707276
___
## Roadmap
### * [Step by step guide for DevOps, SRE or any other Operations Role in 2024](https://roadmap.sh/devops)
### * [DevOps Roadmap for 2024. with learning resources](https://github.com/milanm/DevOps-Roadmap)
---
## Certification
#### * [AWS]()
#### * [Google]()
---
## Table of contents
### I. Linux OS
1. [Command Line](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/linux/cli) - Command Line Interface

### II. Networking
1. [TCP / IP](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/tcp)
2. [LAN]() - Local Network Area
3. [WAN]() - Wide Area Network
4. [Routing]() - The process of sending packets from a host on one network to another host on a different remote network
5. [ARP / RARP / DNS]()
6. [Subnet & Calculator]()
7. [DHCP]() - Dynamic Host Configuration Protocol
8. [DNS]() - Domain Name System
### III.Secure Sockets Layer & Transport Layer Security
1. [SSL / TLS](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/ssl-tls) - Secure Sockets Layer & Transport Layer Security
2. [Let’s Encrypt - Certbot](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/ssl-tls/certbot) - Let’s Encrypt is a free, automated, and open certificate
### IV. HTTP(S) protocol
1. [HTTP1 / HTTP2](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/http) - Hypertext Transfer Protocol
2. [HTTP Request / Respone]()
3. [HTTP Header]()
4. [HTTP Status Code]()
### V. Restful API
1. [API]() - Application Programing Interface
2. [Rest API](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/restful) - Representational State Transfer (REST)
### VI. API gateway, Load Balancer, Reverse-Proxy, Proxy
1. [HAProxy](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/haproxy?ref_type=heads) - The Reliable, High Perf. TCP/HTTP Load Balancer
2. [OpenResty](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/openresty?ref_type=heads)- dynamic web platform based on NGINX and LuaJIT
3. [Kong Gateway](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/kong?ref_type=heads) - API gateway built for hybrid and multi-cloud, optimized for microservices and distributed architectures
4. [Squid](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/squid) - is a caching proxy for the Web supporting HTTP, HTTPS, FTP, and more
### VII. RPC protocol
1. [RPC]() - Remote Procedure Call
2. [gRPC]() - gRPC is a modern open source high performance
### VIII. Container Orchestration
1. [Kubernetes](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/kubernetes) - is an open source container orchestration engine