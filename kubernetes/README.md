# Kubernetes Cluster for production environment with high availability mode installtion step by step
### Need to know:
#### - Container Runtime (CR)
- The container runtime is the low-level component that creates and runs containers
- A container runtime is the foundational software that allows containers to operate within a host system
- Kubernetes supports multiple container runtimes: CRI-O, Docker Engine, containerd, etc...
#### - Container Runtime Interface (CRI)
- It is a set of APIs that allows Kubernetes to interact with different container runtimes
- The CRI defines the API for creating, starting, stopping, and deleting containers, as well as for managing images and container networks.

#### - Open Container Initiative (OCI)
- It is a set of standards for container formats and runtimes.
#### - Container Storage Interface (CSI)
- CSI is an initiative supported by the Cloud Native Computing Foundation (CNCF)
- Kubernetes CSI is a Kubernetes-specific implementation
- The CSI standard determines how arbitrary blocks and file storage systems are exposed to workloads on containerization systems like Kubernetes
#### - Container Network Interface (CNI)
- It is a plugin-based architecture with vendor-neutral specifications and libraries for creating network interfaces for Containers.
- It is not specific to Kubernetes
#### - Classless Inter-Domain Routing (CIDR)
#### - Kubernetes controllers:
- Pod:
  - Pods are the smallest deployable units of computing
  - A Pod is a group of one or more containers, with shared storage and network resources, and a specification for how to run the containers
- Deployment
- Replicaset: it is often used to guarantee the availability of a specified number of identical Pods
- DaemonSet: A DaemonSet ensures that a single instance of a pod is running on each node(worker) in a cluster
- StatefulSets:
  - StatefulSets solve the challenges of running stateful services in Kubernetes.
  - The persistent Pod identities permit storage volumes to be associated with specific Pods inside the StatefulSet
- Service
- Endpoints
- ...
### 1. Cluster Architecture
- That it is a distributed system.
- Has multiple components spread across different servers over a network.
- These servers could be Virtual machines or Bare metal servers. We call it a Kubernetes cluster.
- A Kubernetes cluster consists of control plane nodes and nodes(woker)
- Architecture:
![](./images/k8s.png)
#### 1.1 Control Plane
#### - API Server
![](./images/k8s-api-server.png)
- The central hub of the Kubernetes cluster that exposes the Kubernetes API.
- The communication between the API server and other components in the cluster happens over TLS to prevent unauthorized access to the cluster.
- Responsible:
    + API management: Exposes the cluster API endpoint and handles all API requests
    + Authentication (Using client certificates, bearer tokens, and HTTP Basic Authentication) and Authorization (ABAC and RBAC evaluation)
    + Processing API requests and validating data for the API objects like pods, services, etc.
    + It is the only component that communicates with etcd.
#### - Scheduler
- Stores the resource usage data for each compute node.
- Determines whether new containers should be deployed, and if so, where they should be placed
- How does the Scheduler works:
  + To choose the best node, the Kube-scheduler uses filtering and scoring operations.
  + In filtering, the scheduler finds the best-suited nodes where the pod can be scheduled
  + In the scoring phase, the scheduler ranks the nodes by assigning a score to the filtered worker nodes.
  + Once the node is selected, the scheduler creates a binding event in the API server. Meaning an event to bind a pod and node.
![](./images/k8s-scheduler.png)
#### - Controller Manager
- It manages all the controllers and the controllers try to keep the cluster in the desired state.
- Can extend kubernetes with custom controllers associated with a custom resource definition.
- The Kube scheduler is also a controller managed by the Kube controller manager.
- List of important built-in controllers:
  + Deployment; Replicaset; DaemonSet; Job; CronJob.
  + Endpoints; Namespace; Service Accounts; Node.
- How does the Controller Manager works:
![](./images/k8s-controller-manager.png)
#### - Cloud Controller Manager (CCM)
- The cloud controller manager acts as a bridge between Cloud Platform APIs and the Kubernetes cluster
- Allows Kubernetes cluster to provision cloud resources like instances (for nodes), Load Balancers (for services), and Storage Volumes (for persistent volumes)
![](./images/k8s-cloud-controller-manager.png)
#### - ETCD
- Etcd is an open source, key-value store database
- Stores all configurations, states, and metadata of Kubernetes objects (pods, secrets, daemonsets, deployments, configmaps, statefulsets, etc)
- Etcd exposes key-value API using gRPC. Also, the gRPC gateway is a RESTful proxy that translates all the HTTP API calls into gRPC messages
- Etcd stores all objects under the /registry directory key in key-value format
- Etcd may be configured externally
![](./images/k8s-etcd.png)
#### 1.2 Node
#### - Kubelet
- Kubelet is an agent component that runs on every node in the cluster
- It does not run as a container instead runs as a daemon, managed by systemd
- Responsible:
  + Creating, modifying, and deleting containers for the pod.
  + Handling liveliness, readiness, and startup probes.
  + Mounting volumes by reading pod configuration and creating respective directories on the host for the volume mount.
  + Collecting and reporting Node and pod status via calls to the API server with implementations like cAdvisor and CRI.
- How does the Kubelet works:
  + uses the CRI (container runtime interface) gRPC interface to talk to the container runtime
  + Uses the CSI (container storage interface) gRPC to configure block volumes.
  + Uses the CNI (Container Network Interface) plugin configured in the cluster to allocate the pod IP address and set up any necessary network routes and firewall rules for the pod.
![](./images/k8s-kubelet.png)
#### - Kube proxy
- Need to know:
  + Service object: is a way to expose a set of pods internally or to external traffic.
  + Endpoint object: contains all the IP addresses and ports of pod groups under a Service object.
  + Can't ping the ClusterIP because it is only used for service discovery, unlike pod IPs which are pingable.
- Creates network rules to send traffic to the backend pods (endpoints) grouped under the Service object.
- Handle all the load balancing, and service discovery.
- How does Kube-proxy work:
  + Talks to the API server to get the details about the Service (ClusterIP) and respective pod IPs & ports (endpoints)
  + Uses any one of the following modes to create/update rules for routing traffic to pods: IPTables, IPVS, Kernelspac (only for Windows), Userspace (legacy & not recommended), Nftables(Alpha Feature).
![](./images/k8s-kubeproxy.png)
- How does Kubernetes make use of the container runtime?
    + Example of CRI-O container runtime interface
![](./images/k8s-cr.png)
#### - Pods
- Represents a single instance of an application

### 2. Addon Components:
#### 2.1. CNI Plugin (Container Network Interface)
- How does the CNI Plugin work?
  - Kube-controller-manager is responsible for assigning pod CIDR to each node. Each pod gets a unique IP address from the pod CIDR
  - Kubelet interacts with container runtime to launch the scheduled pod. The CRI plugin which is part of the Container runtime interacts with the CNI plugin to configure the pod network
  - CNI Plugin enables networking between pods spread across the same or different nodes using an overlay network
![](./images/k8s-cni.png)
#### 2.2. CoreDNS (For DNS server)
- A DNS server within the Kubernetes cluster
- DNS-based service discovery
#### 2.3. Metrics Server (For Resource Metrics)
- Collect performance data and resource usage of Nodes and pods in the cluster
#### 2.4. Web UI (Kubernetes Dashboard)
- Dashboard to manage the object via web UI

### 3. Installtion
#### 3.1 [Bare metal / On-premises](https://gitlab.com/nguyennhatkhang2704/ops-document/-/tree/main/kubernetes/manual) - Manual installtion on Bare Metal / On-premises server
#### 3.2 Kubespray

### Reference:
- [Kubernetes](https://kubernetes.io)
- [Kubernetes Architecture Definition](https://avinetworks.com/glossary/kubernetes-architecture)
- [Understanding Kubernetes Architecture: A Comprehensive Guide](https://devopscube.com/kubernetes-architecture-explained)
- [What is Container Runtime Interface (CRI)?](https://www.devopsschool.com/blog/list-of-top-container-runtime-interface-projects)
- [What Is Kubernetes CSI?](https://bluexp.netapp.com/blog/cvo-blg-kubernetes-csi-basics-of-csi-volumes-and-how-to-build-a-csi-driver)
- [A brief overview of the Container Network Interface (CNI) in Kubernetes](https://www.redhat.com/sysadmin/cni-kubernetes)