# Kubernetes on bare metal
### There are typically a few important reasons for use :
1. Cost
2. Compliance & Data Privacy
3. Business Policy
4. Enterprises may not wish to be tied to a single cloud provider
### Challenges:
1. Etcd
2. Load balancing
3. Availability:
- This would mean having multiple master nodes per cluster.
- Multiple Kubernetes clusters across different availability zones.
4. Auto-scaling
- Auto-scaling based on workload needs can help save resources.
- This is difficult to achieve for bare metal Kubernetes clusters unless you are using a bare metal automation platform.
5. Networking
6. Persistent storage
7. Upgrades
- Need to upgrade your clusters roughly every 3 months
- The version upgrade may create issues if there are API incompatibilities introduced with a newer version
8. Monitoring
### Solutions:
1. Etcd: [Production-ready ETCD cluster]()
- Manage highly available etcd cluster
- Need to take frequent backups to ensure business continuity in case the cluster goes down, and the etcd data is lost
2. Load balancing: [MetalLB]()
3. Persistent Storage
- When it comes to bare metal storage, there's nothing more battle tested than Ceph
- Ceph provides a complete CSI implementation: block storage, object storage (S3 compatible), and file storage.
- Longhorn
- [CSI]()
4. Auto-scaling: [Open-source Ironic](), [Platform9’s Managed Bare Metal]()
### Reference:
- [Kubernetes on-premises: why and how](https://platform9.com/blog/kubernetes-on-premises-why-and-how)
- [The challenges of on-premise Kubernetes clusters and how to solve them](https://www.padok.fr/en/blog/on-premise-kubernetes)
- [So You Want to Run Kubernetes On Bare Metal](https://deploy.equinix.com/blog/guide-to-running-kubernetes-on-bare-metal)
- [Deploying a Production Kubernetes Cluster in 2023 — A Complete Guide](https://iampavel.medium.com/deploying-a-production-kubernetes-cluster-in-2023-a-complete-guide-4f31b2dc7c6e)
- [Deploying a high-availability, fault-tolerant Kubernetes Service on bare metal clusters with MetalLB BGP](https://www.redhat.com/en/blog/deploying-a-high-availability-fault-tolerant-kubernetes-service-on-baremetal-clusters-with-metallb-bgp)
- [Choosing a CSI for Kubernetes](https://deploy.equinix.com/developers/guides/choosing-a-csi-for-kubernetes)