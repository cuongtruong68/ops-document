## HTTP
- What is HTTP - Hypertext Transfer Protocol? \
HTTP is a protocol for fetching resources such as HTML documents. It is the foundation of any data exchange on the Web and it is a client-server protocol, which means requests are initiated by the recipient, usually the Web browser. A complete document is reconstructed from the different sub-documents fetched, for instance, text, layout description, images, videos, scripts, and more.
### I. Components of HTTP
#### 1. HTTP request
![](./images/http-request.png)
##### 1.1 URI
- The target of an HTTP request is called a "resource", it can be a document, a photo, or anything else. Each resource is identified by a Uniform Resource Identifier (URI)
- Syntax:
```
- Scheme
- Authority
- Port
- Path
- Query
- Fragment
```
##### 1.2 URL
- The most common form of URI is the Uniform Resource Locator (URL), which is known as the web address.
- Example:
```
https://developer.mozilla.org
https://developer.mozilla.org/en-US/docs/Learn/
https://developer.mozilla.org/en-US/search?q=URL
``` 
##### 1.3 HTTP Request structure:
```
- Request method: GET
- Request URL: /
- HTTP version: HTTP/1.1
- Request header: Host, Accept-Languge
- Request body(message)
```
#### 2. HTTP response
![](./images/http-respone.png)
* HTTP Respone structure:
```
- Respone status code: 200
- Respone status message: OK
- HTTP version: HTTP/1.1
- Respone header: Server, Content-Type
- Respone body(message)
```
#### 3. HTTP header
- HTTP headers let the client and the server pass additional information with an HTTP request or response. An HTTP header consists of its case-insensitive name followed by a colon (:), then by its value. Whitespace before the value is ignored
- Example:
```
- Authorization
- Cache-Control
- Last-Modified
- Accept
```
#### 4. HTTP Messages
- HTTP messages are how data is exchanged between a server and a client. There are two types of messages: requests sent by the client to trigger an action on the server, and responses, the answer from the server.
- Example:
```
POST /api/user HTTP/1.1
Host: test.local
Content-Type: application/json
Authorization: XXX
{
    "age": 10,
    "name": "John Doe"
}
```
#### 5. HTTP request methods
- HTTP defines a set of request methods to indicate the desired action to be performed for a given resource
- Example:
```
- GET, POST, PUT, DELETE
- CONNECT, TRACE, PATCH
- HEAD, OPTIONS
```
#### 6. HTTP response status codes
- HTTP response status codes indicate whether a specific HTTP request has been successfully completed
- Grouped in five classes
```
1. Informational responses (100 – 199)
2. Successful responses (200 – 299)
3. Redirection messages (300 – 399)
4. Client error responses (400 – 499)
5. Server error responses (500 – 599)
```
#### 7. Reference:
1. [An overview of HTTP](https://developer.mozilla.org/en-US/docs/Web/HTTP/Overview)
2. [HTTP request methods](https://developer.mozilla.org/en-US/docs/Web/HTTP/Methods)
3. [HTTP response status codes](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status)
4. [HTTP headers](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers)
5. [HTTP Messages](https://developer.mozilla.org/en-US/docs/Web/HTTP/Messages)
6. [Resources and URIs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Basics_of_HTTP/Identifying_resources_on_the_Web)
### I. HTTP1.1
#### 1. Release of HTTP/1.1 in 1997 until recently, there have been few revisions to the protocol
#### 2. Sample of request/respone with HTTP1.1
![](./images/http-req-res.png)
## II. HTTP2
### 1. The history of HTTP/1.1 to HTTP/2
- Based on the research done by Google, Microsoft, and Facebook, the IETF (Internet Engineering Task Force) released the HTTP/2 protocol in 2015. This became the second major version of the most useful internet protocol, HTTP
### 1. HTTP/2 vs HTTP/1.1
#### 1.1 Compare
![](./images/http2-vs-http1.png)
### 2. Header Compression
![](./images/http2-header-compression.png)
#### 2.1 Static Table Definition
![](./images/http2-satic-header.png)
![](./images/http2-satic-header1.png)
#### 2.2 Dynamic Table
[Reference](https://httpwg.org/specs/rfc7541.html#dynamic.table)
### 3. HTTP/2 handles a message
![](./images/http2-handles-message.png)
### 4. Structure of a frame
![](./images/http2-structure-frame.png)
### 5. Stream
![](./images/stream.png)
### 6. RPC
- RPC Flow \
How a remote procedure call works over the network \
![](./images/RPC-flow.png)
### 7. Protocol Buffers
- What is Protocol Buffer? \
Protocol Buffers are a language-neutral, platform-neutral extensible mechanism for serializing structured data \
It’s like JSON, except it’s smaller and faster, and it generates native language bindings. You define how you want your data to be structured once, then you can use special generated source code to easily write and read your structured data to and from a variety of data streams and using a variety of languages.
Protocol buffers are a combination of the definition language (created in .proto files), the code that the proto compiler generates to interface with data, language-specific runtime libraries, and the serialization format for data that is written to a file (or sent across a network connection).
- Protocol buffer encoded byte stream \
![](./images/protobuf-encoded-byte-stream.png)
- Message Encoding \
```Tag value = (field_index << 3) | wire_type```
- Available wire types and corresponding field types \
![](./images/protobuf-field-type.png)
- Structure of the tag value \
![](./images/structure-of-tag.png)
- Length-Prefixed Message Framing \
![](./images/length-prefix-framing.png)
#### 7. GRPC
- What is gRPC? \
gRPC can use protocol buffers as both its Interface Definition Language (IDL) and as its underlying message interchange format
- gRPC over HTTP/2 \
How gRPC semantics relate to HTTP/2
- Request Message
- Response Message
- Status codes
- Message Flow in gRPC
  * Simple RPC
  * Server-streaming RPC
  * Client-streaming RPC
  * Bidirectional-streaming RPC
#### 8. Reference
1. [HTTP/2 and How it Works](https://cabulous.medium.com/http-2-and-how-it-works-9f645458e4b2)
2. [Chapter 4. gRPC: Under the Hood](https://www.oreilly.com/library/view/grpc-up-and/9781492058328/ch04.html#callout_grpc__under_the_hood_CO1-6)
3. [Analyzing gRPC messages using Wireshark](https://grpc.io/blog/wireshark/)
4. [HPACK: Header Compression for HTTP/2](https://httpwg.org/specs/rfc7541.html)
5. [What is gRPC?](https://grpc.io/docs/what-is-grpc/)
6. [Protobuf Overview](https://protobuf.dev/overview/)
7. [Status codes and their use in gRPC](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)