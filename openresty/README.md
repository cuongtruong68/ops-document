## Customize Docker Openresty
### Reference:
* https://github.com/openresty/docker-openresty
* https://github.com/rongfengliang/openresty-nginx-module-vts
### Modules:
* https://github.com/vozlt/nginx-module-vts
### Build:
#### 1. Alpine
#### 1.1 Add openresty nginx module vts
```
...
# Docker Build Arguments
ARG NGINX_MODULE_VTS_VERSION="0.1.18"
ARG NGINX_MODULE_VTS_URL_BASE="https://github.com/vozlt/nginx-module-vts/archive/v${NGINX_MODULE_VTS_VERSION}.tar.gz"
...
ARG RESTY_CONFIG_OPTIONS="\
    --with-compat \
    ...
    --add-module=/tmp/nginx-module-vts-${NGINX_MODULE_VTS_VERSION} \
...
RUN apk add --no-cache --virtual .build-deps \
        build-base \
        ...
    && curl -fSL ${NGINX_MODULE_VTS_URL_BASE} -o nginx-module-vts-${NGINX_MODULE_VTS_VERSION}.tar.gz \
    && tar xzf nginx-module-vts-${NGINX_MODULE_VTS_VERSION}.tar.gz \
    ...
```
#### 1.2 Custome openresty respone header
``` 
Example HTTP respone:
Server name: KhangNN
Server version: 2023
...
&& sed -i 's@"openresty/"@"KhangNN/"@g' ./bundle/nginx-${NGINX_VERSION}/src/core/nginx.h \
&& sed -i 's@"1.19.3"@"2023"@g' ./bundle/nginx-${NGINX_VERSION}/src/core/nginx.h \
&& sed -i 's/"Server: openresty"/"Server: KhangNN"/g' ./bundle/nginx-${NGINX_VERSION}/src/http/ngx_http_header_filter_module.c \
&& sed -i 's/"Server: " NGINX_VER CRLF/"Server: KhangNN" CRLF/g' ./bundle/nginx-${NGINX_VERSION}/src/http/ngx_http_header_filter_module.c \
...
```
#### 1.3 Build images
```
docker build no-cache -t openresty-alpine:1.19.3.1 .
```
#### 2. Alpine fat
```
docker build no-cache -t openresty-alpine-fat:1.19.3.1 .
```
### Use case:
#### 1. Docker compose ingress 
- [docker-compose.yml](./use-case/docker-compose.yml)

### Configuration:
#### 1. Websocket
```
    map $http_upgrade $connection_upgrade {
        default upgrade;
        '' close;
    }
    ...
    location / {

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection $connection_upgrade;
        proxy_cache_bypass $http_upgrade;
        proxy_buffering off;
        ...
    }
```
#### 2. Allow CORS request
```
map $http_origin $allow_origin {
    ~^https://(.*\.)?mydomain.com$ $http_origin;
    ~^https://(.*\.)?mydoiman1.com$ $http_origin;
    ~^http://localhost(:\d+)?$ $http_origin;
    default "";
}
    ...
location ~ ^/api/ {

    if ($request_method ~* "(GET|POST|PUT|DELETE)") {
      add_header 'Access-Control-Allow-Origin' $allow_origin always;
    }

    if ($request_method = OPTIONS ) {
      access_log off;
      add_header 'Access-Control-Allow-Origin' $allow_origin always;
      add_header "Access-Control-Allow-Methods" "GET, POST, PUT, DELETE, OPTIONS, HEAD";
      add_header "Access-Control-Allow-Headers" "x-access-token, content-type, authorization, origin, accept, token_type, clientapp, accesstoken";
      return 200;
    }

    proxy_max_temp_file_size 0;
    proxy_http_version 1.1;
    proxy_pass http://mybackend;
    proxy_hide_header Access-Control-Allow-Origin;
    proxy_hide_header Access-Control-Allow-Methods;
    proxy_hide_header Access-Control-Allow-Headers;
    proxy_hide_header Access-Control-Allow-Credentials;
    proxy_hide_header x-envoy-upstream-service-time;
    proxy_hide_header x-powered-by;
}

```
#### 3. Content Caching
```
proxy_cache_path  /var/cache/nginx/my-cache levels=1:2 keys_zone=my_cache:20m max_size=80M inactive=7d;

  location  ~ ^/(static|assets)/ {
    proxy_cache_key $scheme$proxy_host$request_uri$http_accept_encoding;
    add_header X-Proxy-Cache $upstream_cache_status;
    proxy_cache my_cache;
    proxy_cache_use_stale error timeout updating http_500 http_502 http_503 http_504;
    proxy_ignore_headers Set-Cookie;

    access_log off;
    expires 7d;
    proxy_max_temp_file_size 0;
    proxy_http_version 1.1;
    proxy_set_header Connection "";
    proxy_hide_header X-Powered-By;
    proxy_pass http://my-service;
  }

```
#### 4. Trim URI
```
  location ~ ^/my-api/ {

    rewrite ^/my-api/(.*) /$1 break;

    proxy_max_temp_file_size 0;
    proxy_http_version 1.1;
    proxy_pass http://my-backend-service;
  }
```
#### 5. Custome respone error
```
  error_page 400 /400.html;
  location /400.html {
    default_type text/plain;
    return 400 '{"code":400,"message": "Bad Request"}';
  }

  error_page 401 /401.html;
  location /401.html {
    default_type text/plain;
    return 401 '{"code":401,"message": "Unauthorized"}';
  }

  error_page 403 /403.html;
  location /403.html {
    default_type text/plain;
    return 403 '{"code":403,"message": "Forbidden"}';
  }
```