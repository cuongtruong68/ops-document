# Let’s Encrypt is a free, automated, and open certificate authority
### The Certbot provides an easy way to generate Let’s Encrypt free certificates
### 1. Install certbot
Centos
```
yum install certbot nginx
```
Ubuntu
```
snap install --classic certbot nginx 
```
### 2. Create certificate
#### 2.1. Run nginx to listening on port 80
Centos
```
systemctl start nginx
```
#### 2.2. Standalone
```
certbot certonly --standalone -d domain1.local -d domain2.local
```
- Needs to bind to port 80 on the server run this command
- Get certificate files after they was created
```
ls -l /etc/letsencrypt/live/domain1.local/
... cert.pem -> ../../
... chain.pem -> ../../
... fullchain.pem -> ../../
... privkey.pem -> ../../
```
#### 2.3. Apache
```
certbot --apache
```
#### 2.4. Nginx
```
certbot --nginx -d domain1.local -d domain2.local
```
### 3. Use certificate
Nginx
```
  ...
  server {
    listen 0.0.0.0:443 ssl;
    server_name domain1.local domain2.local;

    ssl_certificate /etc/letsencrypt/live/domain1.local/fullchain.pem;
    ssl_certificate_key /etc/letsencrypt/live/domain1.local/privkey.pem;

    resolver 8.8.8.8 ipv6=off;
    location / {
        return 200;
    }
  }
  ...
```
### 4. Automatically Renew Certificates
```
crontab -e
0 12 * * * /usr/bin/certbot renew --quiet
```

### Reference:
- [How to Generate Let’s Encrypt SSL using Certbot](https://tecadmin.net/how-to-generate-lets-encrypt-ssl-using-certbot)