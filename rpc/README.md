## RPC
- RPC Flow \
How a remote procedure call works over the network \
![](./images/RPC-flow.png)
### 1. Protocol Buffers
- What is Protocol Buffer? \
Protocol Buffers are a language-neutral, platform-neutral extensible mechanism for serializing structured data \
It’s like JSON, except it’s smaller and faster, and it generates native language bindings. You define how you want your data to be structured once, then you can use special generated source code to easily write and read your structured data to and from a variety of data streams and using a variety of languages.
Protocol buffers are a combination of the definition language (created in .proto files), the code that the proto compiler generates to interface with data, language-specific runtime libraries, and the serialization format for data that is written to a file (or sent across a network connection).
- Protocol buffer encoded byte stream \
![](./images/protobuf-encoded-byte-stream.png)
- Message Encoding \
`Tag value = (field_index << 3) | wire_type`
- Available wire types and corresponding field types \
![](./images/protobuf-field-type.png)
- Structure of the tag value \
![](./images/structure-of-tag.png)
- Length-Prefixed Message Framing \
![](./images/length-prefix-framing.png)
### 2. GRPC
- What is gRPC? \
gRPC can use protocol buffers as both its Interface Definition Language (IDL) and as its underlying message interchange format
- gRPC over HTTP/2 \
How gRPC semantics relate to HTTP/2
- Request Message
- Response Message
- Status codes
- Message Flow in gRPC
  * Simple RPC
  * Server-streaming RPC
  * Client-streaming RPC
  * Bidirectional-streaming RPC
### 3. Reference
1. [HTTP/2 and How it Works](https://cabulous.medium.com/http-2-and-how-it-works-9f645458e4b2)
2. [Chapter 4. gRPC: Under the Hood](https://www.oreilly.com/library/view/grpc-up-and/9781492058328/ch04.html#callout_grpc__under_the_hood_CO1-6)
3. [Analyzing gRPC messages using Wireshark](https://grpc.io/blog/wireshark/)
4. [HPACK: Header Compression for HTTP/2](https://httpwg.org/specs/rfc7541.html)
5. [What is gRPC?](https://grpc.io/docs/what-is-grpc/)
6. [Protobuf Overview](https://protobuf.dev/overview/)
7. [Status codes and their use in gRPC](https://grpc.github.io/grpc/core/md_doc_statuscodes.html)
8. [Remote Procedure Call (RPC)](https://www.techtarget.com/searchapparchitecture/definition/Remote-Procedure-Call-RPC)