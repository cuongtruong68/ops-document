# Common Linux CLI use case
## 1. For loop
### 1.1 Syntax
```
for var in list
do
  command1
done
## or ##
for var in list; do command1; done
```
### 1.2 Use case
```
 for((i=1;i<=10;i+=1)); do echo "for loop $i times"; done
```
## 2. Date
### 2.1 Syntax
Simple
```
date
```
Customize date format
```
date +"%d/%m/%Y"
```
### 2.2 Use case
```
date +"%d-%m-%Y"
date --date='1 days ago'
```
## 3. Save terminal output to a file
### 3.1 Redirect stdout
```
SomeCommand > SomeFile
```
Or append
```
SomeCommand >> SomeFile
```
### 3.2 Redirect stderr
```
SomeCommand &> SomeFile
```
Or append
```
SomeCommand &>> SomeFile
```
### 3.3 Have both stderr and output displayed on the console and in a file
```
SomeCommand 2>&1 | tee SomeFile
```