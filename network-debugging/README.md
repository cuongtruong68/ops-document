## Network tool
- ngrep
- tcpdump
- wireshark

## Reference
1. [ngrep – Network Packet Analyzer for Linux](https://www.geeksforgeeks.org/ngrep-network-packet-analyzer-for-linux/)
2. [tcpdump](https://linux.die.net/man/8/tcpdump)
3. [wireshark](https://www.wireshark.org/)