# Document for Microsoft SQL Server

## Install
### 1. Linux
- Red Hat
- Configure
- Performance best practices
### 2. Windows
### 3. Reference
- [Quickstart: Install SQL Server and create a database on Red Hat](https://learn.microsoft.com/en-us/sql/linux/quickstart-install-connect-red-hat?view=sql-server-ver16&tabs=rhel8)
- [Configure SQL Server on Linux with the mssql-conf tool](https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-configure-mssql-conf?view=sql-server-ver16&source=recommendations)
- [Performance best practices and configuration guidelines for SQL Server on Linux](https://learn.microsoft.com/en-us/sql/linux/sql-server-linux-performance-best-practices?view=sql-server-ver16)
## SQL Server Profiler
SQL Server Profiler is an interface to create and manage traces and analyze and replay trace results.
## Transaction replication
### 1. Config
- [Doc](./docs/SQL_Server_Transactional_Replication.pdf)
### 2. Add new articles to a existing transactional replication
- Step 1:
```
USE SELECT_DATABASE
select immediate_sync,allow_anonymous,* from distribution.dbo.MSpublications;
```
- Step 2: Disable the immediate sync, Allow_anonymous. 
```
USE SELECT_DATABASE

EXEC sp_changepublication
@publication = 'replication-name',
@property = N'allow_anonymous',
@value = 'False'
GO

EXEC sp_changepublication
@publication = 'replication-name',
@property = N'immediate_sync',
@value = 'False'
GO
```
- Step 3: Add new create article to existing publication, by executing below mentioned command or using GUI option.
```
USE SELECT_DATABASE
EXEC sp_addarticle
@publication = 'replication-name',
@article = article-name,
@source_object = article-name
```
- Step 4: Refresh subscriptions
```
exec sp_helpsubscription @publication='replication-name'
```
- Step 5: START Snapshot Agent
![](./images/mssql-start-snapshot-agent.png)
- Step 6: Re-enable the disabled properties, first, immediate_sync and then Allow_anonymous options
```
EXEC sp_changepublication
@publication = N'replication-name',
@property = N'immediate_sync',
@value = 'TRUE'
 
EXEC sp_changepublication
@publication = N'replication-name',
@property = N'allow_anonymous',
@value = 'TRUE'
```
### 3. Initialize SQL Server replication using a database backup
- Step 1: Set the ‘Allow_Initialize_From_Backup’ parameter to true at the publisher.
```
USE [AdventureWorks2016]
GO
DECLARE @publication AS sysname
SET @publication = N'AdventureWorks2016_Publisher_BKP' 
EXEC sp_changepublication 
  @publication = @publication, 
  @property = N'allow_reinitialize_from_backup', 
  @value = 'true'
GO
```
- Step 2: Create a backup
```
BACKUP DATABASE AdventureWorks2016 TO DISK = 'f:\PowerSQL\AdventureWorks2016PublisherDB.bak' WITH FORMAT
```
- Step 3: restore the database backup on the SQL Server Subscriber
```
USE [master]
RESTORE DATABASE [AdventureWorks2016] FROM  DISK = N'F:\PowerSQL\AdventureWorks2016PublisherDB.bak' WITH  FILE = 1,  
MOVE N'AdventureWorks2016_Data' TO N'f:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017\MSSQL\DATA\AdventureWorks2016_Data.mdf', 
 MOVE N'AdventureWorks2016_Log' TO N'g:\Program Files\Microsoft SQL Server\MSSQL14.SQL2017\MSSQL\Data\AdventureWorks2016_Log.ldf',  
 NOUNLOAD,  REPLACE,  STATS = 5
```
### 4. Reference
  * [How to add new articles to a existing transactional replication without snapshot of entire articles](https://www.geopits.com/blog/adding-a-new-articles-to-a-existing-transactional-replication-without-snapshot-of-entire-articles.html)
  * [SQL Server transactional replication: How to reinitialize a subscription using a SQL Server database backup](https://www.sqlshack.com/sql-server-transactional-replication-how-to-reinitialize-a-subscription-using-a-sql-server-database-backup/)
  * [How to Add/Drop articles from existing publications in SQL Server](https://www.sqlshack.com/how-to-add-drop-articles-from-existing-publications-in-sql-server/)
  * [Initialize SQL Server replication using a database backup](https://www.mssqltips.com/sqlservertip/2386/initialize-sql-server-replication-using-a-database-backup/)
## Change Data Capture (CDC)
### 1. Stream changes from your database
### 2. Reference
  * [What is change data capture (CDC)?](https://learn.microsoft.com/en-us/sql/relational-databases/track-changes/about-change-data-capture-sql-server?view=sql-server-ver16)
  * [Debezium connector for SQL Server](https://debezium.io/documentation/reference/stable/connectors/sqlserver.html)


## Database get deadlock
```
DECLARE @xelfilepath NVARCHAR(260)
SELECT @xelfilepath = dosdlc.path
FROM sys.dm_os_server_diagnostics_log_configurations AS dosdlc;
SELECT @xelfilepath = @xelfilepath + N'system_health_*.xel'
 DROP TABLE IF EXISTS  #TempTable
 SELECT CONVERT(XML, event_data) AS EventData
        INTO #TempTable FROM sys.fn_xe_file_target_read_file(@xelfilepath, NULL, NULL, NULL)
         WHERE object_name = 'xml_deadlock_report'
SELECT EventData.value('(event/@timestamp)[1]', 'datetime2(7)') AS UtcTime, 
            CONVERT(DATETIME, SWITCHOFFSET(CONVERT(DATETIMEOFFSET, 
      EventData.value('(event/@timestamp)[1]', 'VARCHAR(50)')), DATENAME(TzOffset, SYSDATETIMEOFFSET()))) AS LocalTime, 
            EventData.query('event/data/value/deadlock') AS XmlDeadlockReport
     FROM #TempTable
     ORDER BY UtcTime DESC;
```

## Database Statistics
```
DECLARE @AllConnections TABLE(
    SPID INT,
    Status VARCHAR(MAX),
    LOGIN VARCHAR(MAX),
    HostName VARCHAR(MAX),
    BlkBy VARCHAR(MAX),
    DBName VARCHAR(MAX),
    Command VARCHAR(MAX),
    CPUTime INT,
    DiskIO INT,
    LastBatch VARCHAR(MAX),
    ProgramName VARCHAR(MAX),
    SPID_1 INT,
    REQUESTID INT
)

INSERT INTO @AllConnections EXEC sp_who2
select SPID,LOGIN,HostName,DBName,CPUTime,DiskIO  from @AllConnections where DBName is not null and DBName <> 'master';
```

## Drop USER from all Database
```
EXECUTE master.sys.sp_MSforeachdb '
	USE [?]; 
    DECLARE @Tsql NVARCHAR(MAX)
    SET @Tsql = ''''

    SELECT @Tsql = CONCAT(''DROP USER '', ''['', d.name, '']'') 
    FROM sys.database_principals d
    JOIN master.sys.server_principals s
        ON s.sid = d.sid
    WHERE s.name = ''delete.user''
	EXEC (@Tsql)
	print ''Did drop at: USE [?]; '' +  @Tsql
'
GO
```
